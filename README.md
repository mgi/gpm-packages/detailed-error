Error source strings are pretty cryptic. The Detailed Error class provides several helper methods that help extract the useful information from an error cluster.

## Contributing

See [Contributing.md](CONTRIBUTING.md) for information on how to submit pull requests. Bugs can be reported using the repositories issue tracker.

#### _This package is implemented with LabVIEW 2017_
